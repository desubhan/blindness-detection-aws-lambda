# this import statement is needed if you want to use the AWS Lambda Layer called "pytorch-v1-py36"
# it unzips all of the pytorch & dependency packages when the script is loaded to avoid the 250 MB unpacked limit in AWS Lambda
try:
    import unzip_requirements
except ImportError:
    pass

import os
import io
import json
import tarfile
import glob
import time
import logging


import boto3
from botocore.client import Config
import requests
import PIL
import base64



import torch
import torchvision
from torch import Tensor
from torch import nn
import logging as log
from typing import Optional # required for "Optional[type]"
import torch.nn.functional as F
from torchvision import models, transforms
torch.nn.Module.dump_patches = True

# load the S3 client when lambda execution context is created

s3 = boto3.client('s3')
classes = {
    0 : 'No DR',
    1 : 'Mild',
    2 : 'Moderate',
    3 : 'Severe',
    4 : 'Proliferative DR'
}




class Flatten(nn.Module):
    "Flatten `x` to a single dimension, often used at the end of a model. `full` for rank-1 tensor"
    def __init__(self, full:bool=False):
        super().__init__()
        self.full = full

    def forward(self, x):
        return x.view(-1) if self.full else x.view(x.size(0), -1)

class AdaptiveConcatPool2d(nn.Module):
    "Layer that concats `AdaptiveAvgPool2d` and `AdaptiveMaxPool2d`." # from pytorch
    def __init__(self, sz:Optional[int]=None): 
        "Output will be 2*sz or 2 if sz is None"
        super().__init__()
        self.output_size = sz or 1
        self.ap = nn.AdaptiveAvgPool2d(self.output_size)
        self.mp = nn.AdaptiveMaxPool2d(self.output_size)
    def forward(self, x): return torch.cat([self.mp(x), self.ap(x)], 1)


def head():
    return \
        nn.Sequential(        
            AdaptiveConcatPool2d(),
            Flatten(),
            nn.BatchNorm1d(2048),
            nn.Dropout(p=0.25, inplace=False),
            nn.Linear(in_features=2048, out_features=512, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(512),
            nn.Dropout(p=0.5, inplace=False),
            nn.Linear(in_features=512, out_features=5, bias=True)
        )

logger = logging.getLogger()
logger.setLevel(logging.INFO)

MODEL_BUCKET=os.environ.get('MODEL_BUCKET')
logger.info(f'Model Bucket is {MODEL_BUCKET}')

MODEL_KEY=os.environ.get('MODEL_KEY')
logger.info(f'Model Prefix is {MODEL_KEY}')

model = None

def lambda_handler(event, context):
  
    global classes
    global model
    
    headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
    }

    # loading model
    if not model:
        logger.info('Loading model from S3')
        obj = s3.get_object(Bucket=MODEL_BUCKET, Key=MODEL_KEY)
        bytestream = io.BytesIO(obj['Body'].read())
        tar = tarfile.open(fileobj=bytestream, mode="r:gz")
        for member in tar.getmembers():
            if member.name.endswith(".txt"):
                print("Classes file is :", member.name)
                f=tar.extractfile(member)
                classes = f.read().splitlines()
                classes = [str(i).split("'")[1] for i in classes]
                print(classes)
            if member.name.endswith(".pth"):
                print("Model file is :", member.name)
                f=tar.extractfile(member)
                print("Loading PyTorch model")
                my_model=torchvision.models.densenet121() 
                modules=list(my_model.children())
                modules.pop(-1)
                temp=nn.Sequential(nn.Sequential(*modules))
                tempchildren=list(temp.children()) 
                tempchildren.append(head())
                model=nn.Sequential(*tempchildren)
                
                weighties = torch.load(io.BytesIO(f.read()))
                model.load_state_dict(weighties['state_dict'])
                model.eval()
                print("Model loaded ")
                
    # input preprocess
    preprocess = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )
    ])
    img_str = json.loads(event["body"])["image"]
    # img_str = event["image"]
    image_base64 = str(img_str).split(',')[1]
    image = base64.b64decode(str(image_base64))       
    img = PIL.Image.open(io.BytesIO(image))
    img_tensor = preprocess(img)
    img_tensor = img_tensor.unsqueeze(0)

    # predict

    logger.info("Calling prediction on model")
    start_time = time.time()
    predict_values = model(img_tensor)
    logger.info("--- Inference time: %s seconds ---" % (time.time() - start_time))
    preds = F.softmax(predict_values, dim=1)
    conf_score, indx = torch.max(preds, dim=1)
    predict_class = classes[indx]
    logger.info(f'Predicted class is {predict_class}')
    logger.info(f'Softmax confidence score is {conf_score.item()}')
    response = {}
    response['class'] = str(predict_class)
    response['confidence'] = conf_score.item()
    
    
    # return {
    #     'statusCode': str(200),
    #     'body': json.dumps(response),
    #     'headers': {
    #         'Content-Type': 'application/json', 
    #         'Access-Control-Allow-Origin': '*' 
    #     }
    # }
    
    return {
        'statusCode': str(200),
        'body': json.dumps(response),
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Methods": "OPTIONS,POST"
        }
    }
    
    
