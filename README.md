# Blindness Detection AWS Lambda Deployment

###### API Link : https://p7sw6e91xf.execute-api.us-east-1.amazonaws.com/Prod/invocations

###### Frontend Link : http://ec2-54-87-94-232.compute-1.amazonaws.com/index.html

###### Dataset Link : https://www.kaggle.com/c/aptos2019-blindness-detection

######  Request Body format


```python
{"image":"base64 encoded png image"}

```
**Example :**

{"image":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAA.....................}

